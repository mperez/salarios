import unittest
from empleado import Empleado

empleadoPepe = Empleado("Pepe", 20000)
empleadoAna = Empleado ("Ana", 30000)

total = empleadoPepe.calcula_impuestos() + empleadoAna.calcula_impuestos()

print(empleadoPepe)
print(empleadoAna)
print("Los impuestos a pagar en total son {:.2f} euros".format(total))

#Con este código puedes probar el código class Empleado

class TestEmpleado (unittest.TestCase):
    def test_construir(self):
        el = Empleado ("nombre", 5000)
        self.assertEqual (el.nomina, 5000)
    def test_impuestos (self):
        el = Empleado("nombre", 5000)
        self.assertEqual (el.calcula_impuestos(), 5)
    def test_str (self):
        el = Empleado("pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", el.__str__())
