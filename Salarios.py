def CalcularImpuestos(nomina ):
    impuesto = nomina * 0.3
    return impuesto
def CalcularCosteSalarial(nomina, impuestos):
    costesalarial = nomina + impuestos
    return costesalarial
def Mensaje (impuesto, coste_salarial, trabajador):
    print("Los impuestos que", trabajador, "debe pagar son de", impuesto, "euros y su coste salarial es de", coste_salarial, "euros.")
dic_empleado = {"Marta" : 30000,
"Juan Chu Tri" : 25000,
"Ana Lisa Melano" : 50000,
"Dolores Delano" : 90000,
"María Juana de los Tulipanes Verdes de la Iglesia de las Carmelitas de la Santa Fe" : 35,
"Juan Francisco de las Vides de Uvas Negras del Norte de Españita" : 1000000,
"Marta Franco España" : 10000,
"Aitor Tilla" : 70000,
"Aitor Menta" : 10,
"Débora Cabezas" : 10000000000000}

for key,value in dic_empleado.items():
    impuesto = CalcularImpuestos(value)
    coste = CalcularCosteSalarial(value, impuesto)
    Mensaje(impuesto, coste, key)